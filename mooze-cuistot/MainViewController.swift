import UIKit
import Alamofire

class MainViewController: UIViewController {

    @IBOutlet var ordersTableView: UITableView!
    let refreshControl = UIRefreshControl()
    var vSpinner : UIView?
    
    @IBOutlet var noContentLabel: UILabel!
    var orders = [Order]()
    var restaurantId = 0

    @IBOutlet var container: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noContentLabel.isHidden = true
        
        self.ordersTableView.delegate = self
        self.ordersTableView.dataSource = self
        self.ordersTableView.register( UINib(nibName: "OngoingOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "ongoingOrderId")
        self.ordersTableView.showsVerticalScrollIndicator = false
        
        refreshControl.tintColor = UIColor.init(hexString: "#00ADEF")
        refreshControl.addTarget(self, action: #selector(refreshOrders(_ :)), for: .valueChanged)
        self.ordersTableView.addSubview(refreshControl)
        
        loadOrders()
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setStatusBarColor(color: UIColor.init(hexString: "#FFFFFF"))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        setStatusBarColor(color: UIColor.init(hexString: "#00ADEF"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refreshOrders(_ refresh: UIRefreshControl) {
        loadOrders()
    }
    
    func loadOrders() {
        self.showSpinner(onView: self.view)
        restaurantId = UserDefaults.standard.integer(forKey: "restaurant_id")
        
        Alamofire.request("http://51.91.120.188:4466/restaurant/\(restaurantId)/orders",
                          method : .get,
                          encoding : JSONEncoding.default
            ).responseJSON { response in
                
                switch(response.result) {
                    case .success :
                        do {
                            self.removeSpinner()
                            let restaurant = try JSONDecoder().decode(Restaurant.self, from: (response.data)!)
                            guard let orders = restaurant.orders as [Order]? else {
                                return
                            }
                            
                            if orders.count <= 0 {
                                self.noContentLabel.isHidden = false
                            }
                            
                            let ongoings = orders.filter { $0.state != "closed" }
                            self.orders = ongoings
                            self.ordersTableView.reloadData()
                        } catch {
                            print(error)
                            self.removeSpinner()
                            self.noContentLabel.isHidden = false
                        }
                    
                    case .failure :
                        self.removeSpinner()
                        self.alert(withTitle: "Erreur", withMessage: "Une erreur est survene lors du chargement des commandes. Veuillez réessayer") { _ in}
                }
                
                self.refreshControl.endRefreshing()
        }
    }
}

extension MainViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.ordersTableView.dequeueReusableCell(withIdentifier: "ongoingOrderId", for: indexPath) as! OngoingOrderTableViewCell
        let order = orders[indexPath.row]
    
        cell.setOrder(order)
        cell.orderStateUpdatedDelegate = self
        
        return cell
    }
}

protocol OrderStateUpdated {
    func onOrderUpdated(orderID id: Int, orderState state: String)
}

extension MainViewController : OrderStateUpdated {
    func onOrderUpdated(orderID id: Int, orderState state: String) {
        self.showSpinner(onView: self.view)
        
        Alamofire.request("http://51.91.120.188:4466/order/state/\(id)?state=" + state,
            method : .put
            ).response { res in
                guard let response = res.response as HTTPURLResponse? else { return }
                
                switch(response.statusCode) {
                    case 200 :
                        self.removeSpinner()
                        self.alert(withTitle: "Message", withMessage: "La commande a été mis à jour avec succès. S'il sagit d'une commande que vous venez d'archiver, veuillez scroller de haut en bas pour la faire disparaître")
                        { alert in
                            print("alert")
                            self.loadOrders()
                        }
                    default:
                        self.removeSpinner()
                        self.alert(withTitle: "Erreur", withMessage: "Une erreur est survene lors de la mise à jour de la commande. Veuillez réessayer"){ alert in
                            self.loadOrders()
                    }
                }
        }
    }
}

extension MainViewController {

        func showSpinner(onView : UIView) {
            let spinnerView = UIView()
            spinnerView.frame = CGRect(x:0, y:0, width: UIScreen.main.bounds.width + 100, height: UIScreen.main.bounds.height)
            
            spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
            
            let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
            ai.hidesWhenStopped = true
            ai.center = spinnerView.center
            ai.frame = CGRect(x:0, y:0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            ai.startAnimating()
            
            DispatchQueue.main.async {
                spinnerView.addSubview(ai)
                onView.addSubview(spinnerView)
            }
            
            vSpinner = spinnerView
        }
        
        func removeSpinner() {
            DispatchQueue.main.async {
                self.vSpinner?.removeFromSuperview()
                self.vSpinner = nil
            }
        }
}

