import UIKit

class ArchivedTableViewCell: UITableViewCell {
    
    var order: Order?
    
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var detailsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setOrder(_ order: Order) {
        self.order = order
        
        self.order = order
        
        guard let orderStarters = order.orderStarters as [OrderStarter]?,
            let orderMains = order.orderMains as [OrderMain]?,
            let orderDesserts = order.orderDesserts as [OrderDessert]?,
            let orderDrinks = order.orderDrinks as [OrderDrink]?,
            let orderSuggestions = order.orderSuggestions as [OrderSuggestion]?,
            let orderMenus = order.orderMenus as [OrderMenu]?
            else { return }
        
        var rawDesc = ""
        var price: Double = 0
        
        rawDesc += "+\(orderStarters.count) entrées\n"
        orderStarters.forEach {
            rawDesc += "    +\($0.starter.name)\n"
            price += $0.starter.price
        }
        
        rawDesc += "+\(orderMains.count) plats\n"
        orderMains.forEach {
            rawDesc += "      +\($0.main.name)\n"
            price += $0.main.price
        }
        
        rawDesc += "+\(orderDesserts.count) desserts\n"
        orderDesserts.forEach {
            rawDesc += "      +\($0.dessert.name)\n"
            price += $0.dessert.price
        }
        
        rawDesc += "+\(orderDrinks.count) boissons\n"
        orderDrinks.forEach {
            rawDesc += "      +\($0.drink.name)\n"
            price += $0.drink.price
        }
        
        rawDesc += "+\(orderSuggestions.count) suggestions\n"
        orderSuggestions.forEach {
            rawDesc += "      +\($0.suggestion.name)\n"
            price += $0.suggestion.price
        }
        
        rawDesc += "+\(orderMenus.count) menus\n"
        orderMenus.forEach {
            
            rawDesc += "      +Size : \($0.menu.size.name)\n"
            
            guard let orderStarters = $0.menu.starters as [Starter]? else { return }
            rawDesc += "      +\(orderStarters.count) entrées\n"
            orderStarters.forEach { rawDesc += "        +\($0.name)\n" }
            
            guard let orderMains = $0.menu.mains as [Main]? else { return }
            rawDesc += "      +\(orderMains.count) plats\n"
            orderMains.forEach { rawDesc += "        +\($0.name)\n" }
            
            
            guard let orderDrinks = $0.menu.drinks as [Drink]? else { return }
            rawDesc += "      +\(orderDrinks.count) boissons\n"
            orderDrinks.forEach { rawDesc += "        +\($0.name)\n" }
            
            guard let orderDesserts = $0.menu.desserts as [Dessert]? else { return }
            rawDesc += "      +\(orderDesserts.count) desserts\n"
            orderDesserts.forEach { rawDesc += "        +\($0.name)\n" }
            
            rawDesc += "\n"
        }
        
        
        self.detailsLabel.text = rawDesc
        self.priceLabel.text = String(format: "%.2f €", price)
        
        self.dateLabel.text = order.createdAt.toFormattedDate(output: "dd/MM/yyyy à HH:mm")
    }
}
