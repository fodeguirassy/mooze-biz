
import UIKit
import Alamofire

class ClosedOrderViewController: UIViewController {
    
    
    @IBOutlet var headerContainer: UIView!
    @IBOutlet var ordersTableView: UITableView!
    @IBOutlet var containerView: UIView!
    var vSpinner : UIView?
    @IBOutlet var noContentLabel: UILabel!
    
    let refreshControl = UIRefreshControl()
    
    @IBOutlet var touchRemoveButton: UIButton!
    @IBOutlet var touchSaveButton: UIButton!
    
    var restaurantID = 0
    var orders = [Order]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.noContentLabel.isHidden = true
        self.ordersTableView.dataSource = self
        self.ordersTableView.delegate = self
        
        self.ordersTableView.register( UINib(nibName: "ArchivedTableViewCell", bundle: nil), forCellReuseIdentifier: "tableViewCellId")
        
        containerView.layer.cornerRadius = 5
        containerView.layer.masksToBounds = true
        containerView.layer.borderWidth = 0.4
        
        //TODO use grey
        //containerView.layer.borderColor = UIColor.flatGray.cgColor
        
        restaurantID = UserDefaults.standard.integer(forKey: "restaurant_id")
        
        self.loadClosedOrders()
        
        refreshControl.tintColor = UIColor.init(hexString: "#00ADEF")
        refreshControl.addTarget(self, action: #selector(refreshClosedOrders(_ :)), for: .valueChanged)
        self.ordersTableView.addSubview(refreshControl)
        
        self.headerContainer.backgroundColor = UIColor.init(hexString: "#00ADEF")
    }
   
    @objc func refreshClosedOrders(_ refresh: UIRefreshControl) {
        self.loadClosedOrders()
    }
    
    func loadClosedOrders() {
        
        self.showSpinner(onView: self.view)
        
        Alamofire.request("http://51.91.120.188:4466/restaurant/\(restaurantID)/orders/closed",
            method : .get,
            encoding : JSONEncoding.default
            ).responseJSON { response in
                
                switch(response.result) {
                case .success :
                    self.removeSpinner()
                    do {
                        let restaurant = try JSONDecoder().decode(Restaurant.self, from: (response.data)!)
                        
                        guard let orders = restaurant.orders as [Order]? else { return }
                        
                        if(orders.count <= 0) {
                            self.noContentLabel.isHidden = false
                            self.touchRemoveButton.isEnabled = false
                            self.touchSaveButton.isEnabled = false
                        }
                        
                        self.orders = orders
                        self.ordersTableView.reloadData()
                        
                    } catch {
                        print(error)
                        self.alert(withTitle: "Erreur", withMessage: "Une erreur est survene lors du chargement des commandes. Veuillez réessayer") {_ in }
                    }
                case .failure :
                    self.removeSpinner()
                    self.alert(withTitle: "Erreur", withMessage: "Une erreur est survene lors du chargement des commandes. Veuillez réessayer") {_ in }
                }
        }
    }
    
    func archiveOrder() {
        
        self.showSpinner(onView: self.view)
        
        Alamofire.request("http://51.91.120.188:4466/restaurant/\(restaurantID)/orders/archive",
            method : .get,
            encoding : JSONEncoding.default
            ).response { response in
                
                self.removeSpinner()
                
                guard let resp = response.response else {
                    self.alert(withTitle: "Erreur", withMessage: "Une erreur est survene lors de l'archivage des commandes. Veuillez réessayer") {_ in }
                    return
                }
                switch(resp.statusCode) {
                case 200 :
                    self.alert(withTitle: "Information", withMessage: "Toutes vos commandes fermées ont été définitvement archivées") { _ in
                        self.noContentLabel.isHidden = false
                    }
                default :
                    self.alert(withTitle: "Erreur", withMessage: "Une erreur est survene lors de l'archivage des commandes. Veuillez réessayer") {_ in }
                }
        }
    }
    
    @IBAction func touchArchive(_ sender: Any) {
        self.archiveOrder()
    }
    
    @IBAction func touchSave(_ sender: Any) {
        alert(withTitle: "Message", withMessage: "Vos commandes ont bien été enregistrées") { _ in }
    }
}

extension ClosedOrderViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.ordersTableView.dequeueReusableCell(withIdentifier: "tableViewCellId", for: indexPath) as! ArchivedTableViewCell
        
        cell.setOrder(self.orders[indexPath.row])
        
        return cell
    }
}

extension ClosedOrderViewController {
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView()
         spinnerView.frame = CGRect(x:0, y:0, width: UIScreen.main.bounds.width + 100, height: UIScreen.main.bounds.height)
        
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.hidesWhenStopped = true
        ai.center = spinnerView.center
        ai.frame = CGRect(x:0, y:0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        ai.startAnimating()

        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}


