import UIKit

class OngoingOrderTableViewCell: UITableViewCell {

    @IBOutlet var orderDescription: UILabel!
    @IBOutlet var orderLifecycle: UIButton!
    
    @IBOutlet var awayOrHere: UILabel!
    @IBOutlet var countDown: UILabel!
    @IBOutlet var clientName: UILabel!
    @IBOutlet var price: UILabel!
    
    var currentLifecycle = OrderLifecycle.unHandled
    var remainingTime = 0
    private var timer = Timer()
    
    var orderStateUpdatedDelegate : OrderStateUpdated?
    var order: Order?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
        orderLifecycle.backgroundColor = UIColor.init(hexString: "#00ADEF")
    }
    
    func setOrderCurrentState(withState state: String) {
        switch state {
        case "launched":
            currentLifecycle = OrderLifecycle.handled
            orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
        case "served":
            currentLifecycle = OrderLifecycle.closed
            orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
        case "closed":
            currentLifecycle = OrderLifecycle.archived
            orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
        default :
            currentLifecycle = OrderLifecycle.unHandled
            orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func touchUpdateOrder(_ sender: Any) {
        
        guard let order = self.order as Order? else { return }
        
        switch currentLifecycle {
        
        case .unHandled:
            currentLifecycle = OrderLifecycle.handled
            orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
            self.launchCountdown()
            orderStateUpdatedDelegate?.onOrderUpdated(orderID: order.id, orderState: "launched")
            
        case .handled:
            currentLifecycle = OrderLifecycle.closed
            orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
            timer.invalidate()
            orderStateUpdatedDelegate?.onOrderUpdated(orderID: order.id, orderState: "served")
        
        case .closed:
            currentLifecycle = OrderLifecycle.archived
            orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
            orderStateUpdatedDelegate?.onOrderUpdated(orderID: order.id, orderState: "closed")
            
        default:
            currentLifecycle = OrderLifecycle.archived
            orderLifecycle.setTitle(currentLifecycle.rawValue, for: .normal)
        }
    }
    
    func setOrder(_ order: Order) {
        
        self.order = order
        
        guard let orderStarters = order.orderStarters as [OrderStarter]?,
            let orderMains = order.orderMains as [OrderMain]?,
            let orderDesserts = order.orderDesserts as [OrderDessert]?,
            let orderDrinks = order.orderDrinks as [OrderDrink]?,
            let orderSuggestions = order.orderSuggestions as [OrderSuggestion]?,
            let orderMenus = order.orderMenus as [OrderMenu]?
            else { return }
        
        var rawDesc = ""
        var price: Double = 0
        
        rawDesc += "+\(orderStarters.count) entrées\n"
        orderStarters.forEach {
            rawDesc += "    +\($0.starter.name)\n"
            price += $0.starter.price
        }
        
        rawDesc += "+\(orderMains.count) plats\n"
        orderMains.forEach {
            rawDesc += "      +\($0.main.name)\n"
            price += $0.main.price
        }
        
        rawDesc += "+\(orderDesserts.count) desserts\n"
        orderDesserts.forEach {
            rawDesc += "      +\($0.dessert.name)\n"
            price += $0.dessert.price
        }
        
        rawDesc += "+\(orderDrinks.count) boissons\n"
        orderDrinks.forEach {
            rawDesc += "      +\($0.drink.name)\n"
            price += $0.drink.price
        }
        
        rawDesc += "+\(orderSuggestions.count) suggestions\n"
        orderSuggestions.forEach {
            rawDesc += "      +\($0.suggestion.name)\n"
            price += $0.suggestion.price
        }
    
        rawDesc += "+\(orderMenus.count) menus\n"
        orderMenus.forEach {
            
            rawDesc += "      +Size : \($0.menu.size.name)\n"
            
            guard let orderStarters = $0.menu.starters as [Starter]? else { return }
            rawDesc += "      +\(orderStarters.count) entrées\n"
            orderStarters.forEach { rawDesc += "        +\($0.name)\n" }
            
            guard let orderMains = $0.menu.mains as [Main]? else { return }
            rawDesc += "      +\(orderMains.count) plats\n"
            orderMains.forEach { rawDesc += "        +\($0.name)\n" }
            
            
            guard let orderDrinks = $0.menu.drinks as [Drink]? else { return }
            rawDesc += "      +\(orderDrinks.count) boissons\n"
            orderDrinks.forEach { rawDesc += "        +\($0.name)\n" }
            
            guard let orderDesserts = $0.menu.desserts as [Dessert]? else { return }
            rawDesc += "      +\(orderDesserts.count) desserts\n"
            orderDesserts.forEach { rawDesc += "        +\($0.name)\n" }
            
            rawDesc += "\n"
        }
        
        
        self.orderDescription.text = rawDesc
        
        self.awayOrHere.text = order.takeAway ? "A emporter" : "Sur place"
        self.price.text = String(format: "%.2f €", price)
        
        let capitalizeLastName = order.orderOwner.lastname.capitalized
        self.clientName.text = "\(order.orderOwner.name) \(capitalizeLastName.prefix(1))."
        
        self.setOrderCurrentState(withState: order.state)
        
        self.countDown.text = "\(order.countDown.toMinutes()) mn"
        self.remainingTime = order.countDown
    }
    
    func launchCountdown() {
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            if self.remainingTime <= 0 { timer.invalidate() }
            self.remainingTime -= 1000
            self.countDown.text = "\(self.remainingTime.toMinutes()) mn"
        }
    }
}

extension Int {
    func toMinutes() -> Int {
        return (self / 1000) / 60
    }
}
enum OrderLifecycle : String {
    case unHandled = "Je prends la commande"
    case handled = "La commande est prête"
    case closed = "Je ferme la commande"
    case archived = "Cette commande est archivée"
}

enum OrderRequestState : String {
    case handled = "launched"
    case served = "served"
    case cosed = "closed"
}
