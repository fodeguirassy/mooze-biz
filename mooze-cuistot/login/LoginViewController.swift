import UIKit
import Alamofire

class LoginViewController: UIViewController {

    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var forgotPassword: UIButton!
    @IBOutlet var connection: UIButton!
    @IBOutlet var register: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailField.delegate = self
        self.passwordField.delegate = self
    }

    @IBAction func touchLogin(_ sender: Any) {
        
        guard let email = emailField.text,
        let password = passwordField.text
        else {
            alert(withTitle: "Erreur", withMessage: "Assurez-vous d'avoir renseigner vos identifiants") {_ in }
            return
        }
                
        Alamofire.request("http://51.91.120.188:4466/user/login/",
                          method : .post,
                          parameters : ["email": email, "password" : password],
                          encoding : JSONEncoding.default
            ).responseJSON { res in
                switch res.result {
                case .success :
                    guard let user = res.result.value as? [String: Any],
                    let properties = user["properties"] as! [[String: Any]]?,
                    let defaultRestaurant = properties[0] as [String: Any]?,
                    let defaultRestaurantId = defaultRestaurant["id"] as? Int,
                    let restaurantName = defaultRestaurant["name"] as? String,
                    let restaurantAddress = defaultRestaurant["address"] as? String
                    else { return }
                    
                    UserDefaults.standard.set(defaultRestaurantId, forKey: "restaurant_id")
                    UserDefaults.standard.set(restaurantName, forKey: "restaurant_name")
                    UserDefaults.standard.set(restaurantAddress, forKey: "restaurant_address")
                    UserDefaults.standard.set(email, forKey: "restaurant_email")
                    
                    self.present(AppTabController().setup(), animated: true)
                
                    
                case .failure :
                    
                    self.alert(withTitle: "Erreur", withMessage: "Une erreur est survenue lors de la connexion. Veuillez réessayer") {_ in }
                }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setStatusBarColor(color: UIColor.init(hexString: "#FFFFFF"))
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       setStatusBarColor(color: UIColor.init(hexString: "#00ADEF"))
    }
}


