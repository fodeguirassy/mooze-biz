//
//  ContactViewController.swift
//  mooze-cuistot
//
//  Created by fofo fofodev on 26/02/2020.
//  Copyright © 2020 MySelf. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {

    @IBOutlet var restauranName: UILabel!
    @IBOutlet var restaurantNameBody: UILabel!
    @IBOutlet var restaurantEmail: UILabel!
    @IBOutlet var restaurantAddress: UILabel!
    @IBOutlet var conainer: UIView!
    @IBOutlet var headerContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.conainer.toRoundedCorners()
        
        let restaurantName = UserDefaults.standard.string(forKey: "restaurant_name")
        self.restauranName.text = restaurantName
        self.restaurantNameBody.text = restaurantName
        self.restaurantAddress.text = UserDefaults.standard.string(forKey: "restaurant_address")
        self.restaurantEmail.text = UserDefaults.standard.string(forKey: "restaurant_email")
        
        self.headerContainer.setAppColorBackground()
        //self.setStatusBarColor(color: UIColor.init(hexString: "#00ADEF"))
        //UIApplication.shared.statusBarStyle = .lightContent
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func touchGoBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
