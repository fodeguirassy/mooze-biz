import UIKit

class CampaignViewController: UIViewController {
    
    @IBOutlet var container: UIView!
    @IBOutlet var headerContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        container.toRoundedCorners()
        self.headerContainer.setAppColorBackground()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func touchGoBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
