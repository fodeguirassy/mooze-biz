
import Foundation
import UIKit

class AppTabController : UITabBarController, UITabBarControllerDelegate {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {}
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {}
    
    func setup() -> UITabBarController {
        
        let tabController = UITabBarController()
        
        //tabController.tabBar.barTintColor = UIColor.init(hexString: "#00ADEF")
        tabController.tabBar.tintColor = UIColor.init(hexString: "#00ADEF")
        
        //tabController.tabBar.layer.cornerRadius = 20
        //tabController.tabBar.layer.masksToBounds = true
        //tabController.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        let ordersTab = UITabBarItem()
        ordersTab.title = "En cours"
        ordersTab.image = UIImage(named: "order_ongoing")
        ordersTab.applyTabBarInsets()
        
        let settingsTab = UITabBarItem()
        settingsTab.title = "Paramètres"
        settingsTab.image = UIImage(named: "account")
        settingsTab.applyTabBarInsets()
        let settingsNavController = UINavigationController()
        settingsNavController.isNavigationBarHidden = true
        
        let closedOrdersTab = UITabBarItem()
        closedOrdersTab.title = "Archives"
        closedOrdersTab.image = UIImage(named: "order_archived")
        closedOrdersTab.applyTabBarInsets()
        
        let mainVC = MainViewController()
        mainVC.tabBarItem = ordersTab
        
        let settingVC = SettingsViewController()
        settingVC.tabBarItem = settingsTab
        settingsNavController.setViewControllers([settingVC], animated: true)
        
        let closedOrdersVC = ClosedOrderViewController()
        closedOrdersVC.tabBarItem = closedOrdersTab
        
        tabController.viewControllers = [mainVC, closedOrdersVC, settingsNavController]
        
        tabController.delegate = self
        return tabController
    }
}

