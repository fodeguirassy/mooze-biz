// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let restaurant = try? newJSONDecoder().decode(Restaurant.self, from: jsonData)

import Foundation

// MARK: - Restaurant
struct Restaurant: Codable {
    let id: Int
    let name, address: String
    let imageURL, type: String
    let latitude, longitude: Double
    let createdAt, updatedAt: String
    let discountID: Int?
    let orders: [Order]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, address, type
        case imageURL = "imageUrl"
        case latitude, longitude, createdAt, updatedAt
        case discountID = "discountId"
        case orders
    }
}

enum AtedAt: String, Codable {
    case the20200203T215747000Z = "2020-02-03T21:57:47.000Z"
}

// MARK: - Order
struct Order: Codable {
    let id, countDown: Int
    let price: Double?
    let checkedOut, takeAway, archived: Bool
    let state: String
    let createdAt, updatedAt: String
    let userID, orderOwnerID: Int?
    let restaurantID: Int
    let orderOwner: User
    let orderStarters: [OrderStarter]
    let orderDrinks: [OrderDrink]?
    let orderMains: [OrderMain]
    let orderDesserts: [OrderDessert]
    let orderSuggestions: [OrderSuggestion]
    let orderMenus: [OrderMenu]?
    let orderComment: OrderComment?
    
    enum CodingKeys: String, CodingKey {
        case id, price, countDown, checkedOut, takeAway, state, createdAt, updatedAt, archived
        case userID = "userId"
        case orderOwnerID = "orderOwnerId"
        case restaurantID = "restaurantId"
        case orderOwner, orderStarters, orderDrinks, orderMains, orderDesserts, orderSuggestions
        case orderMenus
        case orderComment
    }
}

// MARK: - User
struct User: Codable {
    let id: Int
    let name, lastname, email, password: String
    let admin: Bool
    let imageURL: JSONNull?
    let createdAt, updatedAt: String
    
    enum CodingKeys: String, CodingKey {
        case id, name, lastname, email, password, admin
        case imageURL = "imageUrl"
        case createdAt, updatedAt
    }
}

// MARK: - OrderDessert
struct OrderDessert: Codable {
    let id, orderID, dessertID: Int
    let createdAt, updatedAt: String
    let dessert: Dessert
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderID = "orderId"
        case dessertID = "dessertId"
        case createdAt, updatedAt, dessert
    }
}

// MARK: - Dessert
struct Dessert: Codable {
    let id: Int
    let name: String
    let dessertDescription: String?
    let price: Double
    let imageURL: String?
    let createdAt, updatedAt: String
    let restaurantID: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case dessertDescription = "description"
        case price
        case imageURL = "imageUrl"
        case createdAt, updatedAt
        case restaurantID = "restaurantId"
    }
}


// MARK: - OrderSuggestion
struct OrderSuggestion: Codable {
    let id, orderID, suggestionID: Int
    let createdAt, updatedAt: String
    let suggestion: Suggestion
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderID = "orderId"
        case suggestionID = "suggestionId"
        case createdAt, updatedAt, suggestion
    }
}

// MARK: - OrderMain
struct OrderMain: Codable {
    let id, orderID, mainID: Int
    let createdAt, updatedAt: String
    let main: Main
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderID = "orderId"
        case mainID = "mainId"
        case createdAt, updatedAt, main
    }
}

// MARK: - Main
struct Main: Codable {
    let id: Int
    let name, mainDescription: String
    let price: Double
    let imageURL: String
    let createdAt, updatedAt: String
    let restaurantID: Int
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case mainDescription = "description"
        case price
        case imageURL = "imageUrl"
        case createdAt, updatedAt
        case restaurantID = "restaurantId"
    }
}

// MARK: - OrderDrink
struct OrderDrink: Codable {
    let id, orderID, drinkID: Int
    let createdAt, updatedAt: String
    let drink: Drink
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderID = "orderId"
        case drinkID = "drinkId"
        case createdAt, updatedAt, drink
    }
}

// MARK: - Drink
struct Drink: Codable {
    let id: Int
    let name, drinkDescription: String
    let price: Double
    let imageURL: String
    let createdAt, updatedAt: String
    let restaurantID: Int
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case drinkDescription = "description"
        case price
        case imageURL = "imageUrl"
        case createdAt, updatedAt
        case restaurantID = "restaurantId"
    }
}

// MARK: - OrderStarter
struct OrderStarter: Codable {
    let id, orderID, starterID: Int
    let createdAt, updatedAt: String
    let starter: Starter
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderID = "orderId"
        case starterID = "starterId"
        case createdAt, updatedAt, starter
    }
}

// MARK: - Starter
struct Starter: Codable {
    let id: Int
    let name, starterDescription: String
    let price: Double
    let imageURL: String
    let createdAt, updatedAt: String
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case starterDescription = "description"
        case price
        case imageURL = "imageUrl"
        case createdAt, updatedAt
    }
}


// MARK: - Starter
struct Suggestion: Codable {
    let id: Int
    let name : String
    let suggestionDescription : String?
    let price: Double
    let imageURL: String
    let createdAt, updatedAt: String
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case suggestionDescription = "description"
        case price
        case imageURL = "imageUrl"
        case createdAt, updatedAt
    }
}

struct Menu: Codable {
    let id: Int
    let price: Double
    let imageURL: String?
    let createdAt, updatedAt: String
    let restaurantID, sizeID: Int
    let starters: [Starter]?
    let mains: [Main]?
    let desserts: [Dessert]?
    let drinks: [Drink]?
    let size: Size
    
    enum CodingKeys: String, CodingKey {
        case id, price
        case imageURL = "imageUrl"
        case createdAt, updatedAt
        case restaurantID = "restaurantId"
        case sizeID = "sizeId"
        case starters, mains, desserts, drinks
        case size
    }
}

struct OrderMenu: Codable {
    let id: Int
    let orderId: Int
    let menuId: Int
    let createdAt: String
    let updatedAt: String
    let menu: Menu
    
    enum CodingKeys: String, CodingKey {
        case id, orderId, menuId
        case createdAt, updatedAt
        case menu
    }
}

struct Size: Codable {
    let id: Int
    let name: String
    let imageURL: String?
    let createdAt: String
    let updatedAt: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case imageURL = "imageUrl"
        case createdAt, updatedAt, name
    }
}

struct OrderComment: Codable {
    let id: Int
    let createdAt: String
    let updatedAt: String
    let content: String
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
