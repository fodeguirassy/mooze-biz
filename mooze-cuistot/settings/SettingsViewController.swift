//
//  SettingsViewController.swift
//  mooze-cuistot
//
//  Created by fofo fofodev on 08/01/2020.
//  Copyright © 2020 MySelf. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet var headerView: UIView!
    @IBOutlet var createCampaign: UIButton!
    @IBOutlet var myInformation: UIButton!
    @IBOutlet var printer: UIButton!
    @IBOutlet var contact: UIButton!
    @IBOutlet var disconnect: UIButton!
    
    @IBOutlet var restaurantName: UILabel!
    @IBOutlet var containerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //headerView.layer.backgroundColor = UIColor.flatSkyBlue.cgColor
        createCampaign.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        myInformation.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        printer.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        contact.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        disconnect.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        
        containerView.layer.cornerRadius = 5
        containerView.layer.masksToBounds = true
        containerView.layer.borderWidth = 0.4
        //containerView.layer.borderColor = UIColor.flatGray.cgColor
        
        let restaurantName = UserDefaults.standard.string(forKey: "restaurant_name")
        self.restaurantName.text = restaurantName
        
        self.headerView.backgroundColor = UIColor.init(hexString: "#00ADEF")
    }
    
    /*override func viewWillAppear(_ animated: Bool) {
        if let status = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            status.backgroundColor = UIColor.init(hexString: "#00ADEF")
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
    }*/
    
    @IBAction func createCampaign(_ sender: Any) {
        navigationController?.pushViewController(CampaignViewController(), animated: true)
    }
    
    @IBAction func touchDisconnect(_ sender: Any) {
        UserDefaults.standard.set(0, forKey: "restaurant_id")
        self.present(LoginViewController(), animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchContact(_ sender: Any) {
        navigationController?.pushViewController(ContactViewController(), animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
